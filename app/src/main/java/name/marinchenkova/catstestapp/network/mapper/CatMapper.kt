package name.marinchenkova.catstestapp.network.mapper

import name.marinchenkova.catstestapp.model.CatEntity
import name.marinchenkova.catstestapp.model.CatId
import name.marinchenkova.catstestapp.model.CatListItem
import name.marinchenkova.catstestapp.model.CatItemDto
import javax.inject.Inject

class CatMapper @Inject constructor() {

    fun mapDtoToDefaultListItem(from: CatItemDto): CatListItem = CatListItem(
            mapIdToCatId(from.id),
            from.imageUrl
    )

    fun mapIdToCatId(id: String): CatId = CatId(id)

    fun mapListItemToEntity(from: CatListItem): CatEntity = CatEntity(
        from.id,
        from.imageUrl
    )

    fun mapEntityToListItem(from: CatEntity): CatListItem = CatListItem(
            from.id,
            from.imageUrl
    )

}