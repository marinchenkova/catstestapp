package name.marinchenkova.catstestapp.network.factory

import android.util.Log
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

inline fun <reified T> createRetrofitApi(baseUrl: String): T {
    val client = OkHttpClient.Builder()
            .addInterceptor { chain ->
                val request = chain.request()
                        .newBuilder()
                        .addHeader("x-api-key", "29ecfe42-7de6-49ec-bdd7-041670320219")
                        .build()
                chain.proceed(request)
            }
            .build()

    return Retrofit.Builder()
            .baseUrl(baseUrl)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
            .build()
            .create(T::class.java)
}