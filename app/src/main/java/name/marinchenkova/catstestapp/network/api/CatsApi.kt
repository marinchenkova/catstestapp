package name.marinchenkova.catstestapp.network.api

import io.reactivex.rxjava3.core.Single
import name.marinchenkova.catstestapp.model.CatItemDto
import retrofit2.http.GET
import retrofit2.http.Query

interface CatsApi {

    @GET("search")
    fun getAllCats(
        @Query("limit") limit: Int,
        @Query("page") page: Int,
        @Query("order") order: String,
    ): Single<List<CatItemDto>>

}