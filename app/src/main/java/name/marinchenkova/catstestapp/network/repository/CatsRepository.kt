package name.marinchenkova.catstestapp.network.repository

import io.reactivex.rxjava3.core.Single
import name.marinchenkova.catstestapp.model.CatListItem
import name.marinchenkova.catstestapp.network.api.CatsApi
import name.marinchenkova.catstestapp.network.mapper.CatMapper
import name.marinchenkova.catstestapp.model.SortOrder
import javax.inject.Inject

class CatsRepository @Inject constructor(
        private val api: CatsApi,
        private val mapper: CatMapper
) {

    fun getAllCats(limit: Int, page: Int): Single<List<CatListItem>> {
        return api.getAllCats(limit, page, SortOrder.Asc.value)
                .map { list ->
                    list.map {
                        mapper.mapDtoToDefaultListItem(it)
                    }
                }
    }

}