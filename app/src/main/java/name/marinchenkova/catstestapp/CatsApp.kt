package name.marinchenkova.catstestapp

import android.app.Application
import dagger.android.HasAndroidInjector
import name.marinchenkova.catstestapp.di.AppComponent
import name.marinchenkova.catstestapp.di.DaggerAppComponent

class CatsApp : Application(), HasAndroidInjector {

    private lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerAppComponent.builder()
            .application(this)
            .build()
    }

    override fun androidInjector() = appComponent.injector

}