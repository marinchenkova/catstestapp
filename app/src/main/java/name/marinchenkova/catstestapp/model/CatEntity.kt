package name.marinchenkova.catstestapp.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "cats")
data class CatEntity(
    @PrimaryKey
    val id: CatId,
    val imageUrl: String
)