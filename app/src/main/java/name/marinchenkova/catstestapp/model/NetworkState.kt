package name.marinchenkova.catstestapp.model

sealed class NetworkState {

    object Success : NetworkState()
    object Loading : NetworkState()
    data class Error(val message: String) : NetworkState()

}