package name.marinchenkova.catstestapp.model

data class CatListItem(
        val id: CatId,
        val imageUrl: String,
)