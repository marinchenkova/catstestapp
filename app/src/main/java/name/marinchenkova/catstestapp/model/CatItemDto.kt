package name.marinchenkova.catstestapp.model

import com.google.gson.annotations.SerializedName

data class CatItemDto(
    @SerializedName("id")
    val id: String,

    @SerializedName("url")
    val imageUrl: String
)
