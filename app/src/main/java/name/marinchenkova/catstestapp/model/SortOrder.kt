package name.marinchenkova.catstestapp.model

enum class SortOrder(val value: String) {
    Asc("asc"),
    Desc("desc")
}