package name.marinchenkova.catstestapp.model

data class CatId(
    val id: String
)