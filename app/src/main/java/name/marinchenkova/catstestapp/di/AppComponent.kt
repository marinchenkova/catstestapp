package name.marinchenkova.catstestapp.di

import android.app.Application
import dagger.BindsInstance
import dagger.Component
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [
    DataModule::class,
    AppActivityModule::class,
    AndroidSupportInjectionModule::class,
])
interface AppComponent {

    val injector: DispatchingAndroidInjector<Any>

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent

    }

}