package name.marinchenkova.catstestapp.di

import android.app.Application
import android.content.Context
import dagger.Module
import dagger.Provides
import name.marinchenkova.catstestapp.data.db.CatDao
import name.marinchenkova.catstestapp.data.db.CatDatabase
import name.marinchenkova.catstestapp.data.CatFavoriteStorage
import name.marinchenkova.catstestapp.data.PicsStorage
import name.marinchenkova.catstestapp.network.api.BaseUrl
import name.marinchenkova.catstestapp.network.api.CatsApi
import name.marinchenkova.catstestapp.network.factory.createRetrofitApi
import name.marinchenkova.catstestapp.network.mapper.CatMapper
import name.marinchenkova.catstestapp.network.repository.CatsRepository
import name.marinchenkova.catstestapp.presentation.AppActivity
import javax.inject.Singleton

@Module
object DataModule {

    @Singleton
    @JvmStatic
    @Provides
    fun provideContext(application: Application) = application.applicationContext

    @Singleton
    @JvmStatic
    @Provides
    fun provideApi() = createRetrofitApi<CatsApi>(BaseUrl.value)

    @Singleton
    @JvmStatic
    @Provides
    fun provideRepository(api: CatsApi, mapper: CatMapper) = CatsRepository(api, mapper)

    @Singleton
    @JvmStatic
    @Provides
    fun providePicsStorage(context: Context) = PicsStorage(context)

    @Singleton
    @JvmStatic
    @Provides
    fun provideCatDatabase(context: Context) = CatDatabase.getInstance(context)

    @Singleton
    @JvmStatic
    @Provides
    fun provideCatDao(catDatabase: CatDatabase) = catDatabase.catDao()

    @Singleton
    @JvmStatic
    @Provides
    fun provideCatFavoriteStorage(catDao: CatDao) = CatFavoriteStorage(catDao)

}