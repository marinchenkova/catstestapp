package name.marinchenkova.catstestapp.di

import dagger.Module
import dagger.Provides
import dagger.android.ContributesAndroidInjector
import name.marinchenkova.catstestapp.data.CatFavoriteStorage
import name.marinchenkova.catstestapp.network.mapper.CatMapper
import name.marinchenkova.catstestapp.network.repository.CatsRepository
import name.marinchenkova.catstestapp.presentation.AppActivity
import name.marinchenkova.catstestapp.presentation.favorites.FavoritesFragment
import name.marinchenkova.catstestapp.presentation.favorites.FavoritesViewModelFactory
import name.marinchenkova.catstestapp.presentation.discover.DiscoverFragment
import name.marinchenkova.catstestapp.presentation.discover.DiscoverViewModelFactory

@Module
abstract class AppActivityModule {

    @ContributesAndroidInjector
    abstract fun contributeAppActivity(): AppActivity

    @ContributesAndroidInjector
    abstract fun contributeDiscoverFragment(): DiscoverFragment

    @ContributesAndroidInjector
    abstract fun contributeFavoritesFragment(): FavoritesFragment

    @Module
    companion object {

        @JvmStatic
        @Provides
        fun provideFavoritesViewModelFactory(
                catFavoriteStorage: CatFavoriteStorage,
                catMapper: CatMapper
        ) = FavoritesViewModelFactory(catFavoriteStorage, catMapper)

        @JvmStatic
        @Provides
        fun provideDiscoverViewModelFactory(
            repository: CatsRepository,
            catFavoriteStorage: CatFavoriteStorage,
            catMapper: CatMapper
        ) = DiscoverViewModelFactory(repository, catFavoriteStorage, catMapper)

    }

}