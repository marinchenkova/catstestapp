package name.marinchenkova.catstestapp.data.db

import androidx.lifecycle.LiveData
import androidx.paging.DataSource
import androidx.room.*
import name.marinchenkova.catstestapp.model.CatEntity
import name.marinchenkova.catstestapp.model.CatId

@Dao
interface CatDao {

    @Query("SELECT * FROM cats")
    fun getAllFavorites(): DataSource.Factory<Int, CatEntity>

    @Query("SELECT id FROM cats")
    fun getFavoriteIds(): LiveData<List<CatId>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertFavorite(cat: CatEntity)

    @Delete
    fun deleteFavorite(cat: CatEntity)

    @Query("SELECT EXISTS(SELECT id FROM cats WHERE id = :id)")
    fun isInFavorites(id : CatId) : Boolean

}