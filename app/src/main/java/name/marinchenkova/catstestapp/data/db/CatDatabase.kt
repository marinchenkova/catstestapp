package name.marinchenkova.catstestapp.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import name.marinchenkova.catstestapp.model.CatEntity

@Database(
    entities = [CatEntity::class],
    version = 1,
    exportSchema = false
)
@TypeConverters(value = [
    CatConverters::class
])
abstract class CatDatabase : RoomDatabase() {

    abstract fun catDao(): CatDao

    companion object {

        @Volatile
        private var INSTANCE: CatDatabase? = null

        fun getInstance(context: Context): CatDatabase {
            synchronized(this) {
                var instance = INSTANCE

                if (instance == null) {
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        CatDatabase::class.java,
                        "cat_database"
                    )
                        .fallbackToDestructiveMigration()
                        .build()

                    INSTANCE = instance
                }
                return instance
            }
        }

    }

}