package name.marinchenkova.catstestapp.data

import io.reactivex.rxjava3.core.Completable
import name.marinchenkova.catstestapp.data.db.CatDao
import name.marinchenkova.catstestapp.model.CatEntity
import javax.inject.Inject

class CatFavoriteStorage @Inject constructor(
    private val catDao: CatDao
) {

    val allFavorites = catDao.getAllFavorites()
    val favoriteIds = catDao.getFavoriteIds()

    fun changeFavorite(cat: CatEntity): Completable = Completable.fromAction {
        if (catDao.isInFavorites(cat.id)) catDao.deleteFavorite(cat)
        else catDao.insertFavorite(cat)
    }

}