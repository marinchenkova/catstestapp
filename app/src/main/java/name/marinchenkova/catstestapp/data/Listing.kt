package name.marinchenkova.catstestapp.data

import androidx.lifecycle.LiveData
import androidx.paging.PagedList
import name.marinchenkova.catstestapp.model.NetworkState

data class Listing<T>(
    val pagedList: LiveData<PagedList<T>>,
    val networkState: LiveData<NetworkState>,
    val refresh: () -> Unit
)