package name.marinchenkova.catstestapp.data

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import io.reactivex.rxjava3.disposables.CompositeDisposable
import name.marinchenkova.catstestapp.model.CatListItem
import name.marinchenkova.catstestapp.network.repository.CatsRepository

class CatListDataSourceFactory(
    private val repository: CatsRepository,
    private val compositeDisposable: CompositeDisposable
) : DataSource.Factory<Int, CatListItem>() {

    val dataSource = MutableLiveData<CatListDataSource>()

    override fun create(): DataSource<Int, CatListItem> {
        return CatListDataSource(repository, compositeDisposable).apply {
            dataSource.postValue(this)
        }
    }

}