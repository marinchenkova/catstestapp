package name.marinchenkova.catstestapp.data.db

import androidx.room.TypeConverter
import name.marinchenkova.catstestapp.model.CatId

class CatConverters {

    @TypeConverter
    fun idToCatId(id: String): CatId = CatId(id)

    @TypeConverter
    fun catIdToId(catId: CatId): String = catId.id

}