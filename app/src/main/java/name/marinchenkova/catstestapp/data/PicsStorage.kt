package name.marinchenkova.catstestapp.data

import android.Manifest
import android.app.Activity
import android.app.DownloadManager
import android.content.Context
import android.content.Context.DOWNLOAD_SERVICE
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Environment
import android.util.Log
import androidx.core.app.ActivityCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import name.marinchenkova.catstestapp.R
import java.io.File
import java.io.FileOutputStream
import javax.inject.Inject


class PicsStorage @Inject constructor(private val context: Context) {

    private val storageDir = File(
            Environment
                    .getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
                    .toString()
    )

    fun downloadImage(activity: Activity, name: String, url: String) {
        if (!verifyPermissions(activity)) return
        download(name, url)
    }

    private fun download(name: String, url: String) {
        val request = DownloadManager.Request(Uri.parse(url))
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, formatName(name))
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
        request.allowScanningByMediaScanner()

        val manager = context.getSystemService(DOWNLOAD_SERVICE) as DownloadManager
        manager.enqueue(request)
    }

    private fun verifyPermissions(activity: Activity): Boolean {
        val externalMemoryPermission = ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
        )

        if (externalMemoryPermission != PackageManager.PERMISSION_GRANTED) {
            val permission = Manifest.permission.WRITE_EXTERNAL_STORAGE
            ActivityCompat.requestPermissions(activity, arrayOf(permission), 1)
            return false
        }

        return true
    }

    private fun formatName(name: String) = "${context.getString(R.string.app_name)}_$name.jpg"

}