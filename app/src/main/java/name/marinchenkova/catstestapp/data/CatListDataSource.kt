package name.marinchenkova.catstestapp.data

import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.schedulers.Schedulers
import name.marinchenkova.catstestapp.model.CatId
import name.marinchenkova.catstestapp.model.CatListItem
import name.marinchenkova.catstestapp.model.NetworkState
import name.marinchenkova.catstestapp.network.repository.CatsRepository

class CatListDataSource(
    private val repository: CatsRepository,
    private val compositeDisposable: CompositeDisposable
) : PageKeyedDataSource<Int, CatListItem>() {

    val networkState = MutableLiveData<NetworkState>()

    override fun loadInitial(params: LoadInitialParams<Int>, callback: LoadInitialCallback<Int, CatListItem>) {
        load(params.requestedLoadSize, 0) { data, nextPage ->
            callback.onResult(data, null, nextPage)
        }
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, CatListItem>) {
        load(params.requestedLoadSize, params.key) { data, nextPage ->
            callback.onResult(data, nextPage)
        }
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, CatListItem>) {

    }

    private fun load(
        size: Int,
        page: Int,
        callback: (data: List<CatListItem>, nextPage: Int) -> Unit
    ) {
        repository.getAllCats(size, page)
            .subscribeOn(Schedulers.io())
            .doOnSubscribe {
                networkState.postValue(NetworkState.Loading)
            }
            .subscribe(
                { list ->
                    callback.invoke(list, page + 1)
                    networkState.postValue(NetworkState.Success)
                },
                { error ->
                    error.printStackTrace()
                    networkState.postValue(NetworkState.Error(error.localizedMessage))
                }
            )
            .addTo(compositeDisposable)
    }

}