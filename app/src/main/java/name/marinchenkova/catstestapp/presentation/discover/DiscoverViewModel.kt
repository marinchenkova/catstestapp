package name.marinchenkova.catstestapp.presentation.discover

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import io.reactivex.rxjava3.schedulers.Schedulers
import name.marinchenkova.catstestapp.data.*
import name.marinchenkova.catstestapp.model.CatId
import name.marinchenkova.catstestapp.model.CatListItem
import name.marinchenkova.catstestapp.model.NetworkState
import name.marinchenkova.catstestapp.network.mapper.CatMapper
import name.marinchenkova.catstestapp.network.repository.CatsRepository
import name.marinchenkova.catstestapp.presentation.BaseViewModel
import java.util.concurrent.Executors
import javax.inject.Inject

class DiscoverViewModel @Inject constructor(
    private val repository: CatsRepository,
    private val catFavoriteStorage: CatFavoriteStorage,
    private val catMapper: CatMapper
) : BaseViewModel() {

    val cats: LiveData<PagedList<CatListItem>>
    val networkState: LiveData<NetworkState>
    val favoriteIds: LiveData<List<CatId>>
    private val refresh: () -> Unit

    init {
        val config = PagedList.Config.Builder()
            .setInitialLoadSizeHint(8)
            .setPageSize(8)
            .build()
        val factory = CatListDataSourceFactory(repository, compositeDisposable)
        cats = LivePagedListBuilder(factory, config).build()
        networkState = Transformations.switchMap(factory.dataSource) { it.networkState }
        refresh = { factory.dataSource.value?.invalidate() }
        favoriteIds = catFavoriteStorage.favoriteIds
    }

    fun onCatFavoriteChanged(cat: CatListItem) {
        catFavoriteStorage.changeFavorite(catMapper.mapListItemToEntity(cat))
            .subscribeOn(Schedulers.io())
            .subscribe()
            .addTo(compositeDisposable)
    }

    fun refresh() {
        refresh.invoke()
    }

}