package name.marinchenkova.catstestapp.presentation

class Listener<T>(private val onAction: (T) -> Unit) {

    var item: T? = null

    fun perform() {
        item?.also(onAction)
    }

}