package name.marinchenkova.catstestapp.presentation.discover

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.navGraphViewModels
import dagger.android.support.AndroidSupportInjection
import name.marinchenkova.catstestapp.R
import name.marinchenkova.catstestapp.data.PicsStorage
import name.marinchenkova.catstestapp.databinding.FragmentCatlistBinding
import name.marinchenkova.catstestapp.model.CatListItem
import name.marinchenkova.catstestapp.model.NetworkState
import name.marinchenkova.catstestapp.presentation.CatListAdapter
import javax.inject.Inject

class DiscoverFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: DiscoverViewModelFactory
    private val viewModel: DiscoverViewModel by navGraphViewModels(R.id.nav_graph) {
        viewModelFactory
    }

    @Inject
    lateinit var picsStorage: PicsStorage

    private var _binding: FragmentCatlistBinding? = null
    private val binding get() = _binding!!

    private val adapter = CatListAdapter(
        onSaveClick = ::onSaveImage,
        onFavoriteClick = ::onCatFavoriteChanged
    )

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentCatlistBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        subscribe()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun initViews() {
        binding.catsRecycler.adapter = adapter
        binding.tryAgain.setOnClickListener {
            viewModel.refresh()
        }
        registerForContextMenu(binding.catsRecycler)
    }

    private fun subscribe() {
        viewModel.cats.observe(viewLifecycleOwner, { pagedList ->
            adapter.submitList(pagedList)
        })
        viewModel.favoriteIds.observe(viewLifecycleOwner, { ids ->
            adapter.submitFavoriteIds(ids)
        })
        viewModel.networkState.observe(viewLifecycleOwner, { state ->
            when (state) {
                is NetworkState.Success -> {
                    binding.errorLayout.visibility = View.GONE
                }
                is NetworkState.Loading -> {
                    binding.errorLayout.visibility = View.GONE
                }
                is NetworkState.Error -> {
                    binding.errorLayout.visibility = View.VISIBLE
                }
            }
        })
    }

    private fun onSaveImage(cat: CatListItem) {
        picsStorage.downloadImage(requireActivity(), cat.id.id, cat.imageUrl)
    }

    private fun onCatFavoriteChanged(cat: CatListItem) {
        viewModel.onCatFavoriteChanged(cat)
    }

}