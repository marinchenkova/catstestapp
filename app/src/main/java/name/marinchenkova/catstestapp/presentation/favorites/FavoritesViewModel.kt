package name.marinchenkova.catstestapp.presentation.favorites

import androidx.lifecycle.LiveData
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import io.reactivex.rxjava3.schedulers.Schedulers
import name.marinchenkova.catstestapp.data.CatFavoriteStorage
import name.marinchenkova.catstestapp.data.addTo
import name.marinchenkova.catstestapp.model.CatId
import name.marinchenkova.catstestapp.model.CatListItem
import name.marinchenkova.catstestapp.network.mapper.CatMapper
import name.marinchenkova.catstestapp.presentation.BaseViewModel
import javax.inject.Inject

class FavoritesViewModel @Inject constructor(
        private val catFavoriteStorage: CatFavoriteStorage,
        private val catMapper: CatMapper
) : BaseViewModel() {

    val cats: LiveData<PagedList<CatListItem>>
    val favoriteIds: LiveData<List<CatId>>

    init {
        val config = PagedList.Config.Builder()
                .setInitialLoadSizeHint(8)
                .setPageSize(8)
                .build()

        val factory = catFavoriteStorage.allFavorites.map { entity ->
            catMapper.mapEntityToListItem(entity)
        }

        cats = LivePagedListBuilder(factory, config).build()
        favoriteIds = catFavoriteStorage.favoriteIds
    }

    fun onCatFavoriteChanged(cat: CatListItem) {
        catFavoriteStorage.changeFavorite(catMapper.mapListItemToEntity(cat))
                .subscribeOn(Schedulers.io())
                .subscribe()
                .addTo(compositeDisposable)
    }

    fun refresh() {
        cats.value?.dataSource?.invalidate()
    }

}