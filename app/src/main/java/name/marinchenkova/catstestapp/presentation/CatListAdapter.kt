package name.marinchenkova.catstestapp.presentation

import android.graphics.drawable.Drawable
import android.view.*
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import name.marinchenkova.catstestapp.R
import name.marinchenkova.catstestapp.databinding.ItemCatBinding
import name.marinchenkova.catstestapp.model.CatId
import name.marinchenkova.catstestapp.model.CatListItem

class CatListAdapter(
        private val onSaveClick: (CatListItem) -> Unit,
        private val onFavoriteClick: (CatListItem) -> Unit,
        private val onLoadSuccess: (CatListItem) -> Unit = {},
        private val onLoadFailed: (CatListItem) -> Unit = {}
) : PagedListAdapter<CatListItem, CatListAdapter.CatViewHolder>(DiffCallback) {

    private object DiffCallback : DiffUtil.ItemCallback<CatListItem>() {
        override fun areItemsTheSame(oldItem: CatListItem, newItem: CatListItem): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: CatListItem, newItem: CatListItem): Boolean {
            return oldItem == newItem
        }
    }

    private val favoriteIds = mutableListOf<CatId>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CatViewHolder {
        val view = ItemCatBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
        )

        return CatViewHolder(
                view,
                Listener(onSaveClick),
                Listener(onFavoriteClick),
                Listener(onLoadSuccess),
                Listener(onLoadFailed),
        )
    }

    override fun onBindViewHolder(holder: CatViewHolder, position: Int) {
        getItem(position)?.apply {
            holder.bind(this, id in favoriteIds)
        }

    }

    fun submitFavoriteIds(ids: List<CatId>) {
        favoriteIds.clear()
        favoriteIds.addAll(ids)
    }

    class CatViewHolder(
            private val binding: ItemCatBinding,
            private val onSaveClick: Listener<CatListItem>,
            private val onFavoriteClick: Listener<CatListItem>,
            private val onLoadSuccess: Listener<CatListItem>,
            private val onLoadFailed: Listener<CatListItem>,
    ) : RecyclerView.ViewHolder(binding.root), View.OnCreateContextMenuListener {

        private var wasFavorite = false

        init {
            binding.favorite.setOnClickListener {
                onFavoriteClick.perform()
                setFavoriteRes(!wasFavorite)
            }

            binding.content.setOnCreateContextMenuListener(this)
        }

        fun bind(item: CatListItem, isFavorite: Boolean) {
            onFavoriteClick.item = item
            onSaveClick.item = item
            onLoadSuccess.item = item
            onLoadFailed.item = item

            binding.root.visibility = View.VISIBLE
            setFavoriteRes(isFavorite)

            Glide.with(itemView)
                    .load(item.imageUrl)
                    .listener(object : RequestListener<Drawable> {
                        override fun onLoadFailed(
                                e: GlideException?,
                                model: Any?,
                                target: Target<Drawable>?,
                                isFirstResource: Boolean
                        ): Boolean {
                            onLoadFailed.perform()
                            binding.root.visibility = View.GONE
                            return false
                        }

                        override fun onResourceReady(
                                resource: Drawable?,
                                model: Any?,
                                target: Target<Drawable>?,
                                dataSource: DataSource?,
                                isFirstResource: Boolean
                        ): Boolean {
                            onLoadSuccess.perform()
                            binding.root.visibility = View.VISIBLE
                            return false
                        }
                    })
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .into(binding.content)
        }

        override fun onCreateContextMenu(
                menu: ContextMenu?,
                view: View?,
                menuInfo: ContextMenu.ContextMenuInfo?
        ) {
            if (menu == null) return
            val save = menu.add(Menu.NONE, 1, 1, itemView.context.getString(R.string.save_image))
            save.setOnMenuItemClickListener {
                onSaveClick.perform()
                true
            }
        }

        private fun setFavoriteRes(isFavorite: Boolean) {
            wasFavorite = isFavorite

            val favoriteRes =
                    if (isFavorite) R.drawable.ic_favorite_white_24
                    else R.drawable.ic_favorite_border_white_24

            binding.favorite.setImageResource(favoriteRes)
        }

    }

}