package name.marinchenkova.catstestapp.presentation.favorites

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import name.marinchenkova.catstestapp.data.CatFavoriteStorage
import name.marinchenkova.catstestapp.network.mapper.CatMapper
import name.marinchenkova.catstestapp.network.repository.CatsRepository

@Suppress("UNCHECKED_CAST")
class FavoritesViewModelFactory(
        private val catFavoriteStorage: CatFavoriteStorage,
        private val catMapper: CatMapper
) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(FavoritesViewModel::class.java)) {
            return FavoritesViewModel(catFavoriteStorage, catMapper) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }

}